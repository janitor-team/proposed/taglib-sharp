taglib-sharp (2.1.0.0-5) unstable; urgency=medium

  * Team upload.
  * Update watch file
  * Bump to debhelper-compat (= 12)
  * Bump Standards-Version from 4.1.4 to 4.4.0 (no change required)
  * Fix "Updating the taglib-sharp Uploaders list" Remove Jose Carlos Garcia
    Sogo <jsogo@debian.org> from Uploaders (Closes: #862629)

 -- Christopher Hoskin <mans0954@debian.org>  Sat, 03 Aug 2019 08:31:39 +0100

taglib-sharp (2.1.0.0-4) unstable; urgency=medium

  * Team upload.
  * Update watch file
  * Add upstream/metadata
  * Remove trailing whitespace from control and changelog
  * Use secure copyright format uri
  * Add Homepage to control
  * Use canonical, secure (but deprecated) Alioth VCS URLs
  * Remove extrenuous FIXME left in copyright
  * Unique short descriptions for binary packages
  * Bump debhelper compat from 7 to 11
  * remove env variables from rules, no longer required since get-orig-source
    was removed in commit 176ca2827ef1af86cd73fb4aeeaccfc68f65a8c0
  * Bump Standards-Version from 3.9.4 to 4.1.4 (no change required)
  * Add examples/* to copyright file
  * Update VCS URLs for Salsa
  * Complete debian/copyright coverage

 -- Christopher Hoskin <mans0954@debian.org>  Thu, 07 Jun 2018 19:50:42 +0100

taglib-sharp (2.1.0.0-3) unstable; urgency=low

  * Upload to unstable
  * [05142b4] Bump Standards-Version to 3.9.4

 -- Chow Loong Jin <hyperair@debian.org>  Tue, 17 Sep 2013 02:51:01 +0800

taglib-sharp (2.1.0.0-2) experimental; urgency=low

  * [546a91e] Fix up taglib-sharp.dll path (Closes: #687117)

 -- Chow Loong Jin <hyperair@debian.org>  Mon, 10 Sep 2012 09:21:32 +0800

taglib-sharp (2.1.0.0-1) experimental; urgency=low

  * [7de8d49] Imported Upstream version 2.0.5.0
  * [b1fecd3] Drop upstreamed patches and reexport
  * [c9a5f65] Imported Upstream version 2.1.0.0
  * [187b143] Rename package from taglib2.0 to taglib2.1
  * [be46d9d] Drop trailing whitespaces in debian/rules
  * [1d117e4] Bump Standards-Version from to 3.9.3
  * [49e4dc1] Use copyright-format 1.0

 -- Chow Loong Jin <hyperair@debian.org>  Sun, 29 Jul 2012 18:39:39 +0800

taglib-sharp (2.0.4.0-1) unstable; urgency=low

  * [758c995] Imported Upstream version 2.0.4.0
  * [42657cf] No-change Standards-Version bump (3.8.4 -> 3.9.2)
  * [1248b59] Tidy up build-depends.
    Reindent and merge build-deps with build-deps-indep since all packages are
    arch: all.
  * [3aba07e] Add libexiv2-dev build-dep
  * [176ca28] Get rid of get-orig-source.
    Upstream has fixed their tarballs to stop including tests/samples, so we no
    longer need repacked tarballs.
  * [563733d] Drop 02_al2.patch (Override AL in configure instead)
  * [76500b7] Use dh-autoreconf.
    This avoids having to refresh 01_build-system.patch during each update
  * [9ea27f0] Port patches to gbp-pq
  * [38c1206] Patch to fix ICSharpCode.SharpZipLib check
  * [42cd0dc] Update email address
  * [9d952d5] Create empty tests/Makefile.am for autoreconf
  * [ba1c490] Patch to fix ABI breakage in Taglib.Mpeg4.FIleParser

 -- Chow Loong Jin <hyperair@debian.org>  Sun, 05 Feb 2012 23:58:56 +0800

taglib-sharp (2.0.3.7+dfsg-2) unstable; urgency=low

  [ Chow Loong Jin ]
  * [07dc032] Fix Vcs-Browser URL
  * [090d138] Update debian/watch for new banshee homepage

  [ Jo Shields ]
  * [8ab7ff6] Switch from manual quilt to DebSrc 3.0 (quilt)
  * [4041522] Update debian/rules for DH8-compatible "--with cli"

 -- Jo Shields <directhex@apebox.org>  Tue, 24 Jan 2012 01:01:36 +0000

taglib-sharp (2.0.3.7+dfsg-1) unstable; urgency=low

  [ Chow Loong Jin ]
  * debian/control:
    + Set cli-common-dev version requirement to 0.5.7 for cli.{make,pm}

  [ Iain Lane ]
  * New upstream release 2.0.3.7
    + Solution/projects migrated to VS format
    + Fix ID3v2 unsyncing
    + Fix ID3v2.3 exetended header size calculation
    + Make TagLib.File IDisposable (closes any open streams)
    + Don't throw an exception if RVA2 data is partially incorrect
    + Fall back to nunit-console is nunit-console2 not found
    + Add build check for `al` tool
  * debian/patches/*: Refresh

 -- Iain Lane <laney@ubuntu.com>  Sun, 21 Mar 2010 13:39:32 +0000

taglib-sharp (2.0.3.6+dfsg-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + No change bump of Standards-Version from 3.8.3 to 3.8.4

 -- Chow Loong Jin <hyperair@ubuntu.com>  Thu, 25 Feb 2010 03:11:08 +0800

taglib-sharp (2.0.3.4+dfsg-2) unstable; urgency=low

  * debian/rules:
    + Use dh_makeclilibs -V -- upstream maps 2.0.3.0-$version to
      $version's taglib-sharp.dll. As a result, stuff linked against
      $next_version's taglib-sharp.dll will look for $next_version, and will
      not be able to find $version's dll. Hence, packages linked against
      $version should depend on taglib-sharp >= $version. (Closes: #567659)

 -- Chow Loong Jin <hyperair@ubuntu.com>  Sun, 31 Jan 2010 01:00:34 +0800

taglib-sharp (2.0.3.4+dfsg-1) unstable; urgency=low

  * New upstream release
  * debian/control, debian/patches/*, debian/rules:
    + Use quilt and DH 7.0.50
    + Correct spelling mistake "This packages"
  * debian/README.source:
    + Add for quilt patch system

 -- Chow Loong Jin <hyperair@ubuntu.com>  Wed, 27 Jan 2010 14:31:49 +0800

taglib-sharp (2.0.3.3+dfsg-3) unstable; urgency=low

  * debian/monodoc-taglib-manual.postinst:
    + Delete, this is handled by dpkg triggers now

 -- Jo Shields <directhex@apebox.org>  Sat, 26 Dec 2009 23:24:49 +0000

taglib-sharp (2.0.3.3+dfsg-2) unstable; urgency=low

  * debian/control,
    debian/libtaglib2.0-cil.install,
    debian/libtaglib-cil-dev.install:
    + Create unversioned -dev package containing pkg-config file
  * debian/control:
    + Remove obsolete individual library build-deps no longer needed
      with mono-devel 2.4.3

 -- Jo Shields <directhex@apebox.org>  Mon, 23 Nov 2009 14:28:42 +0000

taglib-sharp (2.0.3.3+dfsg-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + Update my email address
    + Update Vcs-* fields to git
    + Bump Standards-Version to 3.8.3
      - Set Section to cli-mono
  * debian/patches/01_build-system.dpatch:
    + Refreshed to patch cleanly
  * debian/copyright:
    + Updated
  * debian/rules:
    + Use mono-csc instead of csc
    + Bump API version

 -- Chow Loong Jin <hyperair@ubuntu.com>  Fri, 09 Oct 2009 03:17:01 +0800

taglib-sharp (2.0.3.2+dfsg-3) unstable; urgency=low

  * Rebuild to get correct clilibs minimal version thanks to
    debhelper and cli-common-dev incompatibilities.

 -- Sebastian Dröge <slomo@debian.org>  Sat, 07 Mar 2009 12:26:50 +0100

taglib-sharp (2.0.3.2+dfsg-2) unstable; urgency=low

  * debian/patches/03_pkg-config-location.dpatch:
    + Fix location in the pkg-config file to fix FTBFS
      of depending packages like banshee.

 -- Sebastian Dröge <slomo@debian.org>  Fri, 06 Mar 2009 10:52:03 +0100

taglib-sharp (2.0.3.2+dfsg-1) unstable; urgency=low

  * DFSG version of taglib-sharp 2.0.3.2
    + Deleted non-free and undistributable sound files in tests/samples/ from
      the tarball.

  [ Chow Loong Jin ]
  * New upstream release
  * debian/patches/01_build-system.dpatch:
    + Refreshed to patch cleanly.
  * debian/libtaglib2.0-cil.install,
    debian/rules:
    + Updated location of taglib-sharp.pc (libdir => datadir)
  * debian/rules:
    + clilibs bump to 2.0.3.2
  * debian/control:
    + Added me to Uploaders
  * debian/watch:
    + Updated to grab from downloads.banshee-project.org

  [ Jo Shields ]
  * debian/control:
    + Add Vcs-* fields

  [ Sebastian Dröge ]
  * debian/control:
    + Use old gnome-sharp for now.

  [ Mirco Bauer ]
  * Upload to Unstable.
  * debian/control:
    + Removed libgnome2.0-cil build-dep as it's only used for some example
      code which are not shipping anyhow.
  * debian/watch:
    + Fixed regex and mangle dfsg suffix.
  * debian/rules:
    + Implemented get-orig-source target with dfsg-ing part.

 -- Mirco Bauer <meebey@debian.org>  Wed, 04 Mar 2009 21:22:34 +0100

taglib-sharp (2.0.3.1-1) experimental; urgency=low

  * New upstream bugfix release:
    + debian/patches/03_file.cs-docs-failure.dpatch,
      debian/patches/04_mp3-header-checking.dpatch,
      debian/patches/05_add-mimetypes.dpatch,
      debian/patches/06_mp4-itunes-invalid-files.dpatch:
      - Dropped, merged upstream.
  * debian/control,
    debian/rules,
    debian/patches/01_build-system.dpatch:
    + Update for the Mono 2.0 transition.
    + Update build dependencies for Gnome# 2.24.

 -- Sebastian Dröge <slomo@debian.org>  Mon, 26 Jan 2009 09:38:26 +0100

taglib-sharp (2.0.3.0-4) experimental; urgency=low

  * debian/patches/06_mp4-itunes-invalid-files.dpatch:
    + Really add this patch and add it to 00list.
  * debian/patches/05_add-mimetypes.dpatch:
    + Don't add an Ogg mimetype twice, it will make taglib-sharp
      completely useless.

 -- Sebastian Dröge <slomo@debian.org>  Mon, 03 Nov 2008 11:24:39 +0100

taglib-sharp (2.0.3.0-3) experimental; urgency=low

  * debian/patches/04_mp3-header-checking.dpatch:
    + Check MP3 headers a bit more to detect more broken streams.
      Patch from upstream SVN.
  * debian/patches/05_add-mimetypes.dpatch:
    + Add some more mimetypes for different formats.
      Patch from upstream SVN.
  * debian/patches/06_mp4-itunes-invalid-files.dpatch:
    + Add workaround for the MP4 detection. Files from Apple's iTunes
      music store are sometimes not valid according to the standard
      but can still be played by ignoring some parts. Patch from upstream
      bugzilla: http://bugzilla.gnome.org/show_bug.cgi?id=551064
  * debian/control:
    + Update Standards-Version to 3.8.0.

 -- Sebastian Dröge <slomo@debian.org>  Sat, 01 Nov 2008 20:14:03 +0100

taglib-sharp (2.0.3.0-2) unstable; urgency=low

  * debian/watch:
    + Add watch file, thanks to Emilio Pozuelo Monfort.
  * debian/patches/01_build-system.dpatch:
    + Don't run XmlInjector at all to fix FTBFS with newer monodoc
      versions (Closes: #475218).

 -- Sebastian Dröge <slomo@debian.org>  Thu, 10 Apr 2008 08:27:06 +0200

taglib-sharp (2.0.3.0-1) unstable; urgency=low

  * New upstream release:
    + debian/control:
      - Drop now unnecessary libmono-dev build dependency.
    + debian/patches/01_build-system.dpatch:
      - Updated for the new version.
      - Updated to move the documentation to "Various" instead of
        "classlib-taglib-sharp" which does not exist.
  * debian/patches/03_file.cs-docs-failure.dpatch:
    + Fix build of the docs.

 -- Sebastian Dröge <slomo@debian.org>  Sat, 12 Jan 2008 04:21:57 +0100

taglib-sharp (2.0.2.0-2) unstable; urgency=low

  * debian/control:
    + Update Standards-Version to 3.7.3, no additional changes needed.
    + Depend on mono-2.0-devel to fix FTBFS (Closes: #458710).
  * debian/rules:
    + Fix clean target to allow building twice from the same
      source tree (Closes: #442740).
  * debian/patches/02_al2.dpatch:
    + Use al2 instead of al for linking the policy dll to remove the
      mscorlib 1.0 dependency.

 -- Sebastian Dröge <slomo@debian.org>  Wed, 02 Jan 2008 19:07:43 +0100

taglib-sharp (2.0.2.0-1) unstable; urgency=low

  * New upstream release, only contains bugfixes.
  * debian/patches/01_build-system.dpatch:
    + Updated for new upstream release.
  * debian/libtaglib2.0-cil.installcligac,
    debian/rules:
    + Also install policy assemblies to be compatible with version 2.0.0.

 -- Sebastian Dröge <slomo@debian.org>  Sun, 08 Jul 2007 18:59:08 +0200

taglib-sharp (2.0.0-2) unstable; urgency=low

  * debian/copyright:
    + Also note the MIT license of docs/MonodocNodeConfig.cs. Thanks to
      Sebastien Bacher for noticing.

 -- Sebastian Dröge <slomo@debian.org>  Thu, 14 Jun 2007 18:35:59 +0200

taglib-sharp (2.0.0-1) unstable; urgency=low

  [ Jose Carlos Garcia Sogo ]
  * First Debian package (Closes: #422726).

  [ Sebastian Dröge ]
  * New upstream release.
  * Overall cleanup.

 -- Sebastian Dröge <slomo@debian.org>  Tue, 12 Jun 2007 09:02:26 +0200
